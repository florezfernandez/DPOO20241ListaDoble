package modelo;

public class Nodo {
	private Estudiante estudiante;
	private Nodo ant;
	private Nodo sig;
	
	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public Nodo getAnt() {
		return ant;
	}

	public void setAnt(Nodo ant) {
		this.ant = ant;
	}

	public Nodo getSig() {
		return sig;
	}

	public void setSig(Nodo sig) {
		this.sig = sig;
	}

	public Nodo(Estudiante estudiante) {
		this.ant = null;
		this.sig = null;
		this.estudiante = estudiante;
	}
	
}
