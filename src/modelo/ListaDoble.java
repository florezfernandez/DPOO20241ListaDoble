package modelo;

import java.util.ArrayList;
import java.util.Observable;

public class ListaDoble extends Observable {
	private Nodo raiz;
	
	public ListaDoble() {
		this.raiz = null;
	}
	
	public void crear(int id, String nombre, String apellido) {
		Estudiante estudiante = new Estudiante(id, nombre, apellido);
		Nodo nuevoNodo = new Nodo(estudiante);
		if(this.raiz == null) {
			this.raiz = nuevoNodo;
		}else {
			Nodo t = this.raiz;
			while(t.getSig() != null) {
				t = t.getSig();
			}
			t.setSig(nuevoNodo);
			nuevoNodo.setAnt(t);
		}
		this.notificar();
	}
	
	public void notificar() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		Nodo t = this.raiz;
		while(t.getSig() != null) {
			ids.add(t.getEstudiante().getId());
			t = t.getSig();	
		}
		ids.add(t.getEstudiante().getId());
		this.setChanged();
		this.notifyObservers(ids);
	}
}











