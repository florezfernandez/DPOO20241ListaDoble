package controlador;

import modelo.ListaDoble;
import vista.PEstado;
import vista.PLienzo;

public class Control {
	private ListaDoble listaDoble;
	
	public Control(PLienzo pLienzo, PEstado pEstado) {
		this.listaDoble = new ListaDoble();
		this.listaDoble.addObserver(pLienzo);
		this.listaDoble.addObserver(pEstado);
	}

	public void crear(int id, String nombre, String apellido) {
		this.listaDoble.crear(id, nombre, apellido);
	}
	
}
