package vista;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PEstado extends JPanel implements Observer {

	private JLabel lInfo;
	
	public PEstado() {
		this.lInfo = new JLabel("Nodos: 0");
		this.setLayout(new FlowLayout());
		this.add(this.lInfo);
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		ArrayList<Integer> numeros = (ArrayList<Integer>)arg;
		this.lInfo.setText("Nodos: " + numeros.size());
	}

}
