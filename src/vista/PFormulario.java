package vista;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controlador.Control;

public class PFormulario extends JPanel{
	private JTextField tId;
	private JTextField tNombre;
	private JTextField tApellido;
	private JButton bCrear;
	private Control control;
	private FPrincipal fPrincipal;
	
	public PFormulario(FPrincipal fPrincipal) {
		this.fPrincipal = fPrincipal;
		this.setLayout(new GridLayout(4,2,10,10));
		this.tId = new JTextField();
		this.tNombre = new JTextField();
		this.tApellido = new JTextField();
		this.bCrear = new JButton("Crear");
		this.bCrear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				crear();
			}
		});
		
		this.add(new JLabel("Id"));
		this.add(this.tId);
		this.add(new JLabel("Nombre"));
		this.add(this.tNombre);
		this.add(new JLabel("Apellido"));
		this.add(this.tApellido);
		this.add(this.bCrear);
		
		this.control = new Control(this.fPrincipal.getpLienzo(), this.fPrincipal.getpEstado());		
	}

	protected void crear() {
		this.control.crear(Integer.parseInt(this.tId.getText()), this.tNombre.getText(), this.tApellido.getText());
		
	}

}
