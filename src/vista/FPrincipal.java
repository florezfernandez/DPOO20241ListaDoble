package vista;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class FPrincipal extends JFrame{
	private PFormulario pFormulario;
	private PLienzo pLienzo;
	private PEstado pEstado;
	
	public PEstado getpEstado() {
		return pEstado;
	}

	public PLienzo getpLienzo() {
		return pLienzo;
	}

	public FPrincipal() {
		this.setSize(800,600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Listas");
		this.setLayout(new BorderLayout());
		this.pLienzo = new PLienzo();
		this.add(this.pLienzo, BorderLayout.CENTER);
		this.pEstado = new PEstado();
		this.add(this.pEstado, BorderLayout.SOUTH);
		this.pFormulario = new PFormulario(this);
		this.add(this.pFormulario, BorderLayout.WEST);		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new FPrincipal();
	}
}
