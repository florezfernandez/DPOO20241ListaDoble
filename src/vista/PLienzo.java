package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class PLienzo extends JPanel implements Observer{

	private ArrayList<Integer> numeros;

	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		if(this.numeros != null) {
			g2d.setColor(Color.BLUE);
			for(int i=0; i<this.numeros.size(); i++) {
				g2d.drawRect(30+(i*120), 30, 90, 30);
				g2d.drawLine(60+(i*120), 30, 60+(i*120), 60);
				g2d.drawLine(90+(i*120), 30, 90+(i*120), 60);
				g2d.drawString(String.valueOf(this.numeros.get(i)), 70+(i*120), 50);
			}
			
		}
		
	}

	@Override
	public void update(Observable o, Object arg) {
		this.numeros = (ArrayList<Integer>)arg;
		repaint();
		
	}
}
